#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be

void logs(char* string)
{
    printf("%s\n",string);
}

// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char plain[50];
    char hash[33];
};

int qComp(struct entry *first,struct entry *sec)
{
    return strcmp(first->hash,sec->hash);
}

int bComp(char *key,struct entry *elem)
{
    return strcmp(key,elem->hash);
}

// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    //opens the file
    FILE *pswrds = fopen(filename,"r");
    if(!pswrds)
    {
        perror("File unable to be opened.");
        exit(1);
    }
    
    
    int arrLen = 10;
    struct entry *dict = malloc(arrLen * sizeof(struct entry));
    
    //
    int entries =0;
    
    char input[PASS_LEN+1];
    while(fgets(input,PASS_LEN,pswrds) != NULL)
    {
        //expand array if needed
        if(entries == arrLen)
        {
            arrLen = (arrLen * 1.4);
            dict = realloc(dict, (arrLen * sizeof(struct entry)));
        }
        
        //chop off newline
        char* newln = strchr(input,'\n');
        if(newln != NULL){*newln = '\0';}
        
        //Initialize entry
        struct entry ent;
        
        //copy input into plaintext 
        strcpy(ent.plain,input);
        
        //Create and store hash,then free it
        char *tempHash = md5(ent.plain,strlen(ent.plain));
        strcpy(ent.hash,tempHash);
        free(tempHash);
        
        //Assign entry to the array;
        dict[entries] = ent;
        entries++;
    }
    
    *size = entries;
    return dict;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int length =0;
    struct entry *dict = read_dictionary(argv[2], &length);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, length, sizeof(struct entry), qComp);
    
    // Partial example of using bsearch, for testing. Delete this line
    // once you get the remainder of the program working.
    // This is the hash for "rockyou".

    // TODO
    // Open the hash file for reading.
    FILE *hashes = fopen(argv[1],"r");

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    char input[33];
    while(fgets(input,33,hashes)!= NULL)
    {
        char* newln = strchr(input,'\n');
        if(newln != NULL){*newln = '\0';}
        
        // If you find it, get the corresponding plaintext dictionary word.
        // Print out both the hash and word.
        struct entry *found = bsearch(input,dict,length,sizeof(struct entry),bComp);
        if(found != NULL)
        {
            printf("%s %s\n",found->hash,found->plain);
        }
        
    }
    // Need only one loop. (Yay!)
    
    //Free Memory
    free(dict);
}
